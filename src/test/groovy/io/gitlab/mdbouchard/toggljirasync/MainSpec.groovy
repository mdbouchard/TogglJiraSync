package io.gitlab.mdbouchard.toggljirasync

import io.gitlab.mdbouchard.toggljirasync.client.jira.JiraClient
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.Fields
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.Issue
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.WorkLog
import io.gitlab.mdbouchard.toggljirasync.client.toggl.TogglClient
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.Project
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.TimeEntry
import io.gitlab.mdbouchard.toggljirasync.config.Config
import io.gitlab.mdbouchard.toggljirasync.config.JiraConfig
import io.gitlab.mdbouchard.toggljirasync.exception.ExitApplicationException
import org.apache.commons.cli.CommandLine
import spock.lang.Specification
import spock.lang.Unroll

import javax.ws.rs.NotFoundException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption

import static java.time.ZonedDateTime.now

class MainSpec extends Specification {
    void "test printVersionInfo"() {
        when:
        Main.printVersionInfo()

        then:
        noExceptionThrown()
    }

    @Unroll
    void "test parseOptions handles options correctly: #args"() {
        when:
        Main.parseOptions(args as String[])

        then:
        noExceptionThrown()

        where:
        args << [
            [],
            ['--start', '2018-11-24'],
            ['--end', '2018-11-24'],
            ['--dryrun'],
            ['--publish'],
            ['--start', '2018-11-24', '--end', '2018-11-24', '--dryrun'],
            ['--start', '2018-11-24', '--end', '2018-11-24', '--publish']
        ]
    }

    @Unroll
    void "test parseOptions handles options that exit the application: #args"() {
        when:
        Main.parseOptions(args as String[])

        then:
        ExitApplicationException ex = thrown(ExitApplicationException)
        assert ex.exitCode == 0

        where:
        args << [
            ['--help'],
            ['--start'],
            ['--end']
        ]
    }

    void "test readConfig properly creates the config file and reads it correctly"() {
        given: 'Temporarily rename local config file, if any'
        Path configFile = Paths.get(Main.CONFIG_FILE)
        Path backupFile = Paths.get("${Main.CONFIG_FILE}.bak")
        moveFileIfExists(configFile, backupFile)
        assert !Files.exists(configFile)

        when: 'The app is first run'
        Main.readConfig()

        then: 'The sample config file is generated and the app exits'
        assert Files.exists(configFile)
        assertFileContents(configFile, Paths.get(getClass().getResource("/${Main.CONFIG_FILE}.sample").toURI()))

        and: 'The app exits'
        ExitApplicationException ex = thrown(ExitApplicationException)
        assert ex.exitCode == 0

        when: 'The app is run again'
        Config config = Main.readConfig()

        then: 'The configuration is parsed without error'
        noExceptionThrown()
        assert config.jira?.uri?.toString() == 'HOSTNAME'
        assert config.jira?.username == 'USERNAME'
        assert config.jira?.password == 'PASSWORD'
        assert config.toggl?.uri?.toString() == 'https://www.toggl.com'
        assert config.toggl?.token == 'TOKEN_HERE'

        cleanup: 'Restore local config file, if any'
        moveFileIfExists(backupFile, configFile)
    }

    void "test initJiraClient sets up provider"() {
        given:
        JiraConfig config = new JiraConfig(
            uri: new URI('http://jira.com'),
            username: 'username',
            password: 'password'
        )

        when:
        JiraClient jiraClient = Main.initJiraClient(config)

        then:
        assert jiraClient.uri == config.uri
        assert jiraClient.username == config.username
        assert jiraClient.password == config.password
    }

    @Unroll
    void "test findAndPublish"() {
        given:
        TogglClient togglClient = Mock(TogglClient) {
            1 * getTimeEntries(startDate, endDate) >> [
                new TimeEntry(
                    start: now().minusMinutes(60),
                    stop: now(),
                    duration: 60,
                    description: 'IFB-123 Test comment'
                )
            ]
        }
        JiraClient jiraClient = Mock(JiraClient) {
            1 * getWorkLogsForUser('IFB-123') >> []
            1 * getIssue('IFB-123') >> new Issue(
                issueNumber: 'IFB-123',
                fields: new Fields(
                    summary: 'Test summary'
                )
            )
        }
        Main main = new Main(togglClient, jiraClient, Main.parseOptions(args as String[]))

        when:
        main.findAndPublish()

        then:
        noExceptionThrown()

        where:
        startDate    | endDate      | args
        null         | null         | ['--publish']
        '2018-11-23' | null         | ['--start', startDate, '--publish']
        null         | '2018-11-24' | ['--end', endDate, '--publish']
        '2018-11-23' | '2018-11-24' | ['--start', startDate, '--end', endDate, '--publish']
    }

    @Unroll
    void "test findTogglEntries"() {
        given:
        String startDate = '2018-11-23'
        String endDate = '2018-11-24'
        TogglClient togglClient = Mock(TogglClient) {
            getTimeEntries(startDate, endDate) >> timeEntries
        }
        Main main = new Main(togglClient, Mock(JiraClient), Mock(CommandLine))

        when:
        Map<String, List<TimeEntry>> results = main.findTogglEntries(startDate, endDate)

        then:
        noExceptionThrown()
        if (foundIssues) {
            assert results
            foundIssues.each { issueNumber, size ->
                assert results[issueNumber]
                assert results[issueNumber].size() == size
            }
        } else {
            assert !results
        }

        where:
        timeEntries << [
            null,
            [],
            [
                null,
                new TimeEntry(),
                new TimeEntry(duration: 0),
                new TimeEntry(duration: 60)
            ],
            [
                new TimeEntry(start: now().minusMinutes(60), duration: 60, description: 'IFB-123')
            ],
            [
                new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-123')
            ],
            [
                new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-123'),
                new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-123')
            ],
            [
                new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-123'),
                new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-123'),
                new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-123')
            ],
            [
                new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-123'),
                new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-456'),
                new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-789')
            ]
        ]
        // Key => Number Found
        foundIssues << [
            [:],
            [:],
            [:],
            [:],
            ['IFB-123': 1],
            ['IFB-123': 2],
            ['IFB-123': 3],
            ['IFB-123': 1, 'IFB-456': 1, 'IFB-789': 1],
        ]
    }

    @Unroll
    void "test filterByJiraWorkLogs"() {
        given:
        JiraClient jiraClient = Mock(JiraClient) {
            getWorkLogsForUser(issueNumber) >> workLogs
            getWorkLogsForUser('notfound') >> {
                throw new NotFoundException('Not found')
            }
        }
        Main main = new Main(Mock(TogglClient), jiraClient, Mock(CommandLine))

        when:
        Map<String, List<TimeEntry>> results = main.filterByJiraWorkLogs(timeEntries)

        then:
        noExceptionThrown()
        if (filteredEntries) {
            assert results.size() == filteredEntries.size()
            filteredEntries.each { issueNumber, size ->
                assert results.containsKey(issueNumber)
                assert results[issueNumber].size() == size
            }
        } else {
            assert !results
        }

        where:
        issueNumber << [
            null,
            null,
            'IFB-123',
            'IFB-123',
            'IFB-123',
            'IFB-456',
            null
        ]
        timeEntries << [
            null,
            [:],
            [
                'IFB-123': [
                    new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-123')
                ]
            ],
            [
                'IFB-123': [
                    new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-123')
                ]
            ],
            [
                'IFB-123': [
                    new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 59, description: 'IFB-123')
                ]
            ],
            [
                'IFB-123': [
                    new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-123')
                ],
                'IFB-456': [
                    new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-456')
                ]
            ],
            [
                'IFB-123' : [
                    new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-123')
                ],
                'notfound': [
                    new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'notfound')
                ],
                'IFB-456' : [
                    new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, description: 'IFB-456')
                ]
            ]
        ]
        workLogs << [
            null,
            [],
            [],
            [
                // NOT Filtered out because this Toggl entry is more than 1 minute outside of JIRA entry
                new WorkLog(started: now().minusMinutes(120), timeSpentSeconds: 60)
            ],
            [
                // Filtered out because this Toggl entry is within 1 minute of the JIRA entry
                new WorkLog(started: now().minusMinutes(60), timeSpentSeconds: 60)
            ],
            [],
            null
        ]
        // Key => Number Found
        filteredEntries << [
            [:],
            [:],
            ['IFB-123': 1],
            ['IFB-123': 1],
            ['IFB-123': 0],
            ['IFB-123': 1, 'IFB-456': 1],
            ['IFB-123': 1, 'IFB-456': 1]
        ]
    }

    void "test mapTimeEntriesToWorkLogs"() {
        given:
        TogglClient togglClient = Mock(TogglClient) {
            getProjects() >> [
                new Project(id: 1, name: 'Project 1'),
                new Project(id: 2, name: 'Project 2')
            ]
        }
        JiraClient jiraClient = Mock(JiraClient) {
            getIssue(_) >> { String issueNumber ->
                return new Issue(
                    issueNumber: issueNumber,
                    fields: new Fields(
                        summary: 'Test summary'
                    )
                )
            }
        }

        Main main = new Main(togglClient, jiraClient, Mock(CommandLine))

        Map<String, List<TimeEntry>> timeEntries = [
            'IFB-1'  : [
                new TimeEntry(start: now().minusMinutes(360), stop: now(), duration: 60, projectId: null, description: 'IFB-1')
            ],
            'IFB-123': [
                new TimeEntry(start: now().minusMinutes(180), stop: now(), duration: 60, projectId: 1, description: 'IFB-123')
            ],
            'IFB-789': [
                new TimeEntry(start: now().minusMinutes(60), stop: now(), duration: 60, projectId: 1, description: 'IFB-789')
            ],
            'IFB-456': [
                new TimeEntry(start: now().minusMinutes(120), stop: now(), duration: 60, projectId: 2, description: 'IFB-456')
            ],
            'IFB-999': [
                new TimeEntry(start: now(), stop: now(), duration: 60, projectId: 2, description: 'IFB-999')
            ]
        ]

        when:
        List<WorkLog> results = main.mapTimeEntriesToWorkLogs(timeEntries)

        then:
        noExceptionThrown()
        assert timeEntries.size() == results?.size()
        assert results.collect { it.issue.issueNumber } == ['IFB-999', 'IFB-789', 'IFB-456', 'IFB-123', 'IFB-1']
        assert results.collectEntries { [(it.issue.issueNumber): it.comment] } == [
            'IFB-1'  : null,
            'IFB-123': 'Project 1',
            'IFB-456': 'Project 2',
            'IFB-789': 'Project 1',
            'IFB-999': 'Project 2'
        ]
    }

    @Unroll
    void "test publishWorkLogs"() {
        given:
        JiraClient jiraClient = Mock(JiraClient) {
            (jiraCalls) * addWorkLog(_)
        }
        Main main = new Main(Mock(TogglClient), jiraClient, Main.parseOptions(args as String[]))

        when:
        main.publishWorkLogs(workLogs as List<WorkLog>)

        then:
        noExceptionThrown()

        where:
        args << [
            [],
            ['--dryrun'],
            ['--publish'],
            ['--dryrun'],
            ['--publish'],
            ['--dryrun'],
            ['--publish'],
            ['--publish']
        ]
        workLogs << [
            null,
            null,
            null,
            [],
            [],
            [
                new WorkLog(
                    issue: new Issue(
                        issueNumber: 'IFB-123',
                        fields: new Fields(
                            summary: 'Test summary'
                        )
                    ),
                    started: now().minusMinutes(60),
                    timeSpentSeconds: 60,
                    comment: 'Test comment'
                )
            ],
            [
                new WorkLog(
                    issue: new Issue(
                        issueNumber: 'IFB-123',
                        fields: new Fields(
                            summary: 'Test summary'
                        )
                    ),
                    started: now().minusMinutes(60),
                    timeSpentSeconds: 60,
                    comment: 'Test comment'
                )
            ],
            [
                new WorkLog(
                    issue: new Issue(
                        issueNumber: 'IFB-123',
                        fields: new Fields(
                            summary: 'Test summary'
                        )
                    ),
                    started: now().minusMinutes(180),
                    timeSpentSeconds: 30,
                    comment: 'Test comment'
                ),
                new WorkLog(
                    issue: new Issue(
                        issueNumber: 'IFB-456',
                        fields: new Fields(
                            summary: 'Test summary'
                        )
                    ),
                    started: now().minusMinutes(90),
                    timeSpentSeconds: 30,
                    comment: 'Test comment'
                ),
                new WorkLog(
                    issue: new Issue(
                        issueNumber: 'IFB-789',
                        fields: new Fields(
                            summary: 'Test summary'
                        )
                    ),
                    started: now().minusMinutes(60),
                    timeSpentSeconds: 60,
                    comment: 'Test comment'
                )
            ]
        ]
        jiraCalls << [
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            3
        ]
    }

    private static void moveFileIfExists(Path src, Path dest) {
        if (Files.exists(src)) {
            Files.move(src, dest, StandardCopyOption.REPLACE_EXISTING)
        }
    }

    private static void assertFileContents(Path left, Path right) {
        assert Files.readAllBytes(left) == Files.readAllBytes(right)
    }
}
