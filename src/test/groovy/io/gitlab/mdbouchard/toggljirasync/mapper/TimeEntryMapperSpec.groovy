package io.gitlab.mdbouchard.toggljirasync.mapper

import io.gitlab.mdbouchard.toggljirasync.client.jira.api.Fields
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.Issue
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.WorkLog
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.Project
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.TimeEntry
import spock.lang.Specification
import spock.lang.Unroll

import java.time.ZonedDateTime

class TimeEntryMapperSpec extends Specification {
    @Unroll
    void "test toWorkLog properly converts TimeEntry -> WorkLog"() {
        given:
        Issue issue = new Issue(
            issueNumber: issueNumber,
            fields: new Fields(
                summary: 'Test summary'
            )
        )
        TimeEntry timeEntry = new TimeEntry(
            id: 123,
            projectId: projectId,
            description: description,
            start: ZonedDateTime.now().minusSeconds(duration),
            stop: ZonedDateTime.now(),
            duration: duration
        )
        List<Project> projects = [
            new Project(id: 1, name: 'Project 1'),
            new Project(id: 2, name: 'Project 2'),
            new Project(id: 3, name: 'Project 3'),
        ]

        TimeEntryMapper mapper = new TimeEntryMapper(projects)

        when:
        WorkLog workLog = mapper.toWorkLog(issue, timeEntry)

        then:
        noExceptionThrown()
        assert workLog
        assert workLog.issue?.issueNumber == issueNumber
        assert workLog.started == timeEntry.start
        assert workLog.timeSpentSeconds == expectedDuration
        assert workLog.comment == expectedComment

        where:
        issueNumber | projectId | description                     | duration | expectedComment        | expectedDuration
        'ifb-123'   | null      | "${issueNumber} Test comment"   | 0        | 'Test comment'         | 60
        'IfB-123'   | null      | "${issueNumber} Test comment"   | 30       | 'Test comment'         | 60
        'ifB-123'   | null      | "${issueNumber} Test comment"   | 59       | 'Test comment'         | 60
        'IFB-123'   | 1         | "${issueNumber}"                | 60       | "Project ${projectId}" | 120
        'IFB-123'   | 2         | "${issueNumber}"                | 61       | "Project ${projectId}" | 120
        'IFB-123'   | 3         | "${issueNumber}"                | 90       | "Project ${projectId}" | 120
        'IFB-123'   | null      | "${issueNumber}"                | 119      | null                   | 120
        'IFB-123'   | null      | "${issueNumber} : Test comment" | 120      | 'Test comment'         | 180
    }

    @Unroll
    void "test toWorkLog handles NULL projects"() {
        given:
        Issue issue = new Issue(
            issueNumber: issueNumber,
            fields: new Fields(
                summary: 'Test summary'
            )
        )
        TimeEntry timeEntry = new TimeEntry(
            id: 123,
            projectId: projectId,
            description: description,
            start: ZonedDateTime.now().minusSeconds(duration),
            stop: ZonedDateTime.now(),
            duration: duration
        )

        TimeEntryMapper mapper = new TimeEntryMapper(null)

        when:
        WorkLog workLog = mapper.toWorkLog(issue, timeEntry)

        then:
        noExceptionThrown()
        assert workLog
        assert workLog.issue?.issueNumber == issueNumber
        assert workLog.started == timeEntry.start
        assert workLog.timeSpentSeconds == expectedDuration
        assert workLog.comment == expectedComment

        where:
        issueNumber | projectId | description                     | duration | expectedComment | expectedDuration
        'ifb-123'   | null      | "${issueNumber} Test comment"   | 0        | 'Test comment'  | 60
        'IfB-123'   | null      | "${issueNumber} Test comment"   | 30       | 'Test comment'  | 60
        'ifB-123'   | null      | "${issueNumber} Test comment"   | 59       | 'Test comment'  | 60
        'IFB-123'   | 1         | "${issueNumber}"                | 60       | null            | 120
        'IFB-123'   | 2         | "${issueNumber}"                | 61       | null            | 120
        'IFB-123'   | 3         | "${issueNumber}"                | 90       | null            | 120
        'IFB-123'   | null      | "${issueNumber}"                | 119      | null            | 120
        'IFB-123'   | null      | "${issueNumber} : Test comment" | 120      | 'Test comment'  | 180
    }

    @Unroll
    void "test toWorkLog properly handles bad parameters"() {
        given:
        TimeEntryMapper mapper = new TimeEntryMapper(null)

        when:
        mapper.toWorkLog(issue, timeEntry)

        then:
        thrown(IllegalArgumentException)

        where:
        issue                             | timeEntry
        null                              | null
        new Issue()                       | null
        new Issue(issueNumber: 'IFB-123') | null
        new Issue(issueNumber: 'IFB-123') | new TimeEntry()
        new Issue(issueNumber: 'IFB-123') | new TimeEntry(duration: null)
    }

    @Unroll
    void "test prepareComment translates '#comment' to '#expectedComment'"() {
        expect:
        assert TimeEntryMapper.prepareComment(issueNumber, comment) == expectedComment

        where:
        issueNumber | comment                                   | expectedComment
        null        | null                                      | null
        null        | '  '                                      | ''
        ''          | '  '                                      | ''
        'IFB-123'   | "${issueNumber}"                          | ''
        'IFB-123'   | "${issueNumber}: Test 1"                  | 'Test 1'
        'IFB-123'   | "${issueNumber} : Test 2"                 | 'Test 2'
        'IFB-123'   | "${issueNumber}- Test 3"                  | 'Test 3'
        'IFB-123'   | "${issueNumber} ; Test 4"                 | 'Test 4'
        'IFB-123'   | "${issueNumber}  - Test 5"                | 'Test 5'
        'IFB-123'   | "${issueNumber}  -  Test 6"               | 'Test 6'
        'IFB-123'   | "${issueNumber}: Test 7"                  | 'Test 7'
        'IFB-123'   | "${issueNumber}: Test 8"                  | 'Test 8'
        'IFB-123'   | "Test 9 ${issueNumber}"                   | 'Test 9'
        'IFB-123'   | "  Test  10  ${issueNumber}  "            | 'Test 10'
        'IFB-123'   | "${issueNumber} Some  comment   IFB-456 " | 'Some comment IFB-456'
    }

    @Unroll
    void "test roundUpOneMinute rounds #seconds to #expectedSeconds"() {
        expect:
        assert TimeEntryMapper.roundUpOneMinute(seconds) == expectedSeconds

        where:
        seconds | expectedSeconds
        0       | 60
        1       | 60
        30      | 60
        59      | 60
        60      | 120
        61      | 120
        90      | 120
        119     | 120
        120     | 180
    }
}
