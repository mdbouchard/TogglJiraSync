package io.gitlab.mdbouchard.toggljirasync.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import lombok.Getter;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.net.URI;

@Getter
public abstract class RestClient {
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
        .registerModule(new JavaTimeModule())
        .setSerializationInclusion(JsonInclude.Include.NON_NULL)
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    protected final URI uri;
    protected final String username;
    protected final String password;
    protected final WebTarget webTarget;

    public RestClient(final URI uri, final String username, final String password) {
        this.uri = uri;
        this.username = username;
        this.password = password;
        this.webTarget = initClient();
    }

    protected WebTarget initClient() {
        HttpAuthenticationFeature authFeature = HttpAuthenticationFeature.basic(username, password);
        JacksonJsonProvider jsonProvider = new JacksonJsonProvider(OBJECT_MAPPER);

        ClientConfig clientConfig = new ClientConfig()
            .register(authFeature)
            .register(jsonProvider);

        return ClientBuilder.newClient(clientConfig)
            .target(uri);
    }
}
