package io.gitlab.mdbouchard.toggljirasync.client.toggl

import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.junit.WireMockRule
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.Project
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.TimeEntry
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.Workspace
import io.gitlab.mdbouchard.toggljirasync.config.TogglConfig
import org.junit.Rule
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId
import java.time.ZonedDateTime

import static com.github.tomakehurst.wiremock.client.WireMock.equalTo
import static com.github.tomakehurst.wiremock.client.WireMock.get
import static com.github.tomakehurst.wiremock.client.WireMock.ok
import static com.github.tomakehurst.wiremock.client.WireMock.okJson
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import static io.gitlab.mdbouchard.toggljirasync.client.RestClient.OBJECT_MAPPER
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME

class TogglClientSpec extends Specification {
    private static final String TOKEN = 'foo'
    private static final String PASSWORD = 'api_token'
    private static final int PORT = 8080

    @Shared
    TogglClient togglClient

    void setupSpec() {
        TogglConfig togglConfig = new TogglConfig(
            uri: new URI("http://localhost:${PORT}"),
            token: TOKEN
        )
        togglClient = new TogglClient(togglConfig)
    }

    @Rule
    WireMockRule wireMock = new WireMockRule(options()
        .port(PORT)
        .notifier(new ConsoleNotifier(true))
    )

    void "test getDefaultWorkspace fetches the first workspace"() {
        given:
        Workspace workspace1 = new Workspace(
            id: 123,
            name: 'workspace1'
        )
        Workspace workspace2 = new Workspace(
            id: 456,
            name: 'workspace2'
        )
        stubFor(get(urlEqualTo('/api/v8/workspaces'))
            .withBasicAuth(TOKEN, PASSWORD)
            .willReturn(okJson(OBJECT_MAPPER.writeValueAsString([workspace1, workspace2])))
        )

        when:
        Workspace response = togglClient.getDefaultWorkspace()

        then:
        noExceptionThrown()
        assert response
        assert response.id == workspace1.id
        assert response.name == workspace1.name
    }

    void "test getDefaultWorkspace handles no workspaces"() {
        given:
        stubFor(get(urlEqualTo('/api/v8/workspaces'))
            .withBasicAuth(TOKEN, PASSWORD)
            .willReturn(ok())
        )

        when:
        Workspace response = togglClient.getDefaultWorkspace()

        then:
        noExceptionThrown()
        assert !response
    }

    void "test getWorkspaces fetches workspaces"() {
        given:
        Workspace workspace = new Workspace(
            id: 123,
            name: 'workspace'
        )

        stubFor(get(urlEqualTo('/api/v8/workspaces'))
            .withBasicAuth(TOKEN, PASSWORD)
            .willReturn(okJson(OBJECT_MAPPER.writeValueAsString([workspace])))
        )

        when:
        List<Workspace> response = togglClient.getWorkspaces()

        then:
        noExceptionThrown()
        assert response?.size() == 1
        response?.each {
            assert it.id == workspace.id
            assert it.name == workspace.name
        }
    }

    void "test getProjects fetches projects"() {
        given:
        Workspace workspace = new Workspace(
            id: 123,
            name: 'workspace'
        )
        Project project = new Project(
            id: 456,
            name: 'project'
        )

        stubFor(get(urlEqualTo('/api/v8/workspaces'))
            .withBasicAuth(TOKEN, PASSWORD)
            .willReturn(okJson(OBJECT_MAPPER.writeValueAsString([workspace])))
        )

        stubFor(get(urlEqualTo("/api/v8/workspaces/${workspace.id}/projects"))
            .withBasicAuth(TOKEN, PASSWORD)
            .willReturn(okJson(OBJECT_MAPPER.writeValueAsString([project])))
        )

        when:
        List<Project> response = togglClient.getProjects()

        then:
        noExceptionThrown()
        assert response?.size() == 1
        response?.each {
            assert it.id == project.id
            assert it.name == project.name
        }
    }

    void "test getProjects fetches projects by workspace"() {
        given:
        Workspace workspace = new Workspace(
            id: 123,
            name: 'workspace'
        )
        Project project = new Project(
            id: 456,
            name: 'project'
        )

        stubFor(get(urlEqualTo("/api/v8/workspaces/${workspace.id}/projects"))
            .withBasicAuth(TOKEN, PASSWORD)
            .willReturn(okJson(OBJECT_MAPPER.writeValueAsString([project])))
        )

        when:
        List<Project> response = togglClient.getProjects(workspace)

        then:
        noExceptionThrown()
        assert response?.size() == 1
        response?.each {
            assert it.id == project.id
            assert it.name == project.name
        }
    }

    void "test getProjects handles NULL workspace"() {
        when:
        List<Project> result = togglClient.getProjects(null)

        then:
        noExceptionThrown()
        assert result?.isEmpty()
    }

    void "test getTimeEntries fetches entries by date range"() {
        given:
        String dateString = '2018-11-22'
        ZonedDateTime startDate = LocalDate.parse(dateString).atTime(LocalTime.MIN).atZone(ZoneId.systemDefault())
        ZonedDateTime endDate = LocalDate.parse(dateString).atTime(LocalTime.MAX).atZone(ZoneId.systemDefault())

        TimeEntry timeEntry = new TimeEntry(
            id: 123,
            projectId: 456,
            description: 'Test comment',
            start: ZonedDateTime.now().minusMinutes(2),
            stop: ZonedDateTime.now(),
            duration: 120
        )

        stubFor(get(urlPathEqualTo('/api/v8/time_entries'))
            .withBasicAuth(TOKEN, PASSWORD)
            .withQueryParam('start_date', equalTo(startDate.format(ISO_OFFSET_DATE_TIME)))
            .withQueryParam('end_date', equalTo(endDate.format(ISO_OFFSET_DATE_TIME)))
            .willReturn(okJson(OBJECT_MAPPER.writeValueAsString([timeEntry])))
        )

        when:
        List<TimeEntry> response = togglClient.getTimeEntries(dateString, dateString)

        then:
        noExceptionThrown()
        assert response?.size() == 1
        response?.each {
            assert it.id == timeEntry.id
            assert it.projectId == timeEntry.projectId
            assert it.description == timeEntry.description
            assert it.start.isEqual(timeEntry.start)
            assert it.stop.isEqual(timeEntry.stop)
            assert it.duration == timeEntry.duration
        }
    }

    @Unroll
    void "test parseDateString properly parses date strings"() {
        when:
        ZonedDateTime result = TogglClient.parseDateString(date, time)

        then:
        assert result == expectedDate.atTime(time).atZone(ZoneId.systemDefault())

        where:
        date         | time          | expectedDate
        '2018-11-22' | LocalTime.MIN | LocalDate.parse(date)
        '2018-11-22' | LocalTime.MAX | LocalDate.parse(date)
        ''           | LocalTime.MIN | LocalDate.now()
        ''           | LocalTime.MAX | LocalDate.now()
        null         | LocalTime.MIN | LocalDate.now()
        null         | LocalTime.MAX | LocalDate.now()
    }

    void "test formattedDateTime properly converts date to ISO standard"() {
        given:
        String dateString = '2018-11-22T19:04:31Z'
        ZonedDateTime dateTime = ZonedDateTime.parse(dateString)

        expect:
        assert TogglClient.formattedDateTime(dateTime) == dateString
    }
}
