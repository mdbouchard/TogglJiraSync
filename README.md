# TogglJiraSync
A simple tool that publishes time entries from Toggl to JIRA.

### Download
1. Download the [latest release](https://gitlab.com/mdbouchard/TogglJiraSync/-/jobs/artifacts/master/download?job=dist) artifacts.
2. Open the artifacts zip file and extract `build/distributions/toggljirasync-shadow-VERSION.zip` file to a known location.
3. Open a terminal to that known location and browse to the `/bin` directory.
4. Run `toggljirasync`.
