package io.gitlab.mdbouchard.toggljirasync.client.jira.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.util.regex.Pattern;

@Getter
@ToString
public class Issue {
    public static final Pattern ISSUE_PATTERN = Pattern.compile(".*?((?<!([A-Z]{1,10})-?)[A-Z]{1,10}-\\d+).*",
        Pattern.CASE_INSENSITIVE);

    @JsonProperty("key")
    private String issueNumber;

    @JsonProperty
    private Fields fields;
}
