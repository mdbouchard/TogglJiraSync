package io.gitlab.mdbouchard.toggljirasync.client.jira.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.ZonedDateTime;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class WorkLog {
    @JsonIgnore
    private Issue issue;

    @JsonProperty
    private Author author;

    @JsonProperty
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS[Z]")
    private ZonedDateTime started;

    @JsonProperty
    private Long timeSpentSeconds;

    @JsonProperty
    private String comment;
}
