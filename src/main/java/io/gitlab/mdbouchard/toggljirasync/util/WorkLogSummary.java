package io.gitlab.mdbouchard.toggljirasync.util;

import io.gitlab.mdbouchard.toggljirasync.client.jira.api.WorkLog;
import org.apache.commons.collections4.CollectionUtils;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.defaultIfBlank;
import static org.apache.commons.lang3.StringUtils.repeat;

public class WorkLogSummary {
    private static final int ISSUE_SUMMARY_WIDTH = 62;
    private static final int START_TIME_WIDTH = 20;
    private static final int DURATION_WIDTH = 5;
    private static final int COMMENT_WIDTH = 50;
    private static final int PADDING_WIDTH = 2;
    private static final String RULE = "-";
    private static final String RULE_FORMAT = "+%s+%s+%s+%s+";
    private static final String ROW_FORMAT = "| %-" + ISSUE_SUMMARY_WIDTH + "s | %-" + START_TIME_WIDTH + "s | %-" + DURATION_WIDTH + "s | %-" + COMMENT_WIDTH + "s |";
    private static final String DEFAULT_BLANK_COMMENT = "(no description)";
    private static final String ISSUE_DISPLAY_FORMAT = "%s: %s";
    private static final String DURATION_FORMAT = "%02d:%02d";
    private static final String ELLIPSIS = "...";

    public static void report(final String actionMessage, final List<WorkLog> workLogs) {
        report(actionMessage, workLogs, null);
    }

    public static void report(final String message, final List<WorkLog> workLogs,
                              final ProcessCallback<WorkLog> processWorkLog) {
        System.out.println();
        System.out.println(message);

        if (CollectionUtils.isNotEmpty(workLogs)) {
            printHeader();
            for (WorkLog workLog : workLogs) {
                printRow(workLog);
                if (processWorkLog != null) {
                    processWorkLog.process(workLog);
                }
            }
            printFooter();
        } else {
            System.out.println("Nothing to do!");
        }
    }

    public static String defaultCommentIfBlank(final String description) {
        return defaultIfBlank(description, DEFAULT_BLANK_COMMENT);
    }

    static void printRule() {
        System.out.println(String.format(
            RULE_FORMAT,
            repeat(RULE, ISSUE_SUMMARY_WIDTH + PADDING_WIDTH),
            repeat(RULE, START_TIME_WIDTH + PADDING_WIDTH),
            repeat(RULE, DURATION_WIDTH + PADDING_WIDTH),
            repeat(RULE, COMMENT_WIDTH + PADDING_WIDTH)));
    }

    static void printHeader() {
        printRule();
        System.out.println(String.format(ROW_FORMAT, "Issue", "Started", "Time", "Comment"));
        printRule();
    }

    static void printRow(final WorkLog workLog) {
        if (workLog == null) {
            throw new IllegalArgumentException("WorkLog cannot be null!");
        }
        System.out.println(String.format(
            ROW_FORMAT,
            truncate(String.format(ISSUE_DISPLAY_FORMAT, workLog.getIssue().getIssueNumber(),
                workLog.getIssue().getFields().getSummary()), ISSUE_SUMMARY_WIDTH),
            formatDateTime(workLog.getStarted()),
            formatTime(workLog.getTimeSpentSeconds()),
            truncate(defaultCommentIfBlank(workLog.getComment()), COMMENT_WIDTH)
        ));
    }

    static void printFooter() {
        printRule();
    }

    static String formatDateTime(final ZonedDateTime dateTime) {
        return Optional
            .ofNullable(dateTime)
            .map(dt -> dt.withZoneSameInstant(ZoneId.systemDefault())
                .format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
            .orElse(null);
    }

    static String formatTime(final long seconds) {
        long minutes = Math.floorDiv(seconds, 60) % 60;
        long hours = Math.floorDiv(seconds, (60 * 60));
        return String.format(DURATION_FORMAT, hours, minutes);
    }

    static String truncate(final String str, final int maxLength) {
        if (str != null && str.length() > maxLength) {
            return str.substring(0, Math.max(maxLength - ELLIPSIS.length(), 0)) + ELLIPSIS;
        }
        return str;
    }
}
