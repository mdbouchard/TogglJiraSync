package io.gitlab.mdbouchard.toggljirasync.client.toggl;

import io.gitlab.mdbouchard.toggljirasync.client.RestClient;
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.Project;
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.TimeEntry;
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.Workspace;
import io.gitlab.mdbouchard.toggljirasync.config.TogglConfig;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class TogglClient extends RestClient {
    private static final String WORKSPACE_ID_TEMPLATE = "workspaceId";

    private static final String BASE_URI = "/api/v8";
    private static final String WORKSPACE_URI = BASE_URI + "/workspaces";
    private static final String PROJECT_URI = WORKSPACE_URI + "/{" + WORKSPACE_ID_TEMPLATE + "}/projects";
    private static final String TIME_ENTRY_URI = BASE_URI + "/time_entries";

    private static final String API_PASSWORD = "api_token";
    private static final String START_DATE_PARAM = "start_date";
    private static final String END_DATE_PARAM = "end_date";

    public TogglClient(final TogglConfig config) {
        super(config.getUri(), config.getToken(), API_PASSWORD);
    }

    static ZonedDateTime parseDateString(final String dateString, final LocalTime localTime) {
        return (StringUtils.isNotEmpty(dateString)
            ? LocalDate.parse(dateString)
            : LocalDate.now())
            .atTime(localTime)
            .atZone(ZoneId.systemDefault());
    }

    static String formattedDateTime(final ZonedDateTime dateTime) {
        return dateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public Workspace getDefaultWorkspace() {
        return Optional.ofNullable(getWorkspaces())
            .map(workspaces -> workspaces.get(0))
            .orElse(null);
    }

    public List<Workspace> getWorkspaces() {
        return webTarget
            .path(WORKSPACE_URI)
            .request(MediaType.APPLICATION_JSON_TYPE)
            .get(new GenericType<List<Workspace>>() {});
    }

    public List<Project> getProjects() {
        return getProjects(getDefaultWorkspace());
    }

    public List<Project> getProjects(final Workspace workspace) {
        if (workspace != null) {
            return webTarget
                .path(PROJECT_URI)
                .resolveTemplate(WORKSPACE_ID_TEMPLATE, workspace.getId())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(new GenericType<List<Project>>() {});
        }
        return Collections.emptyList();
    }

    public List<TimeEntry> getTimeEntries(final String startDate, final String endDate) {
        // Parse dates
        ZonedDateTime startDateTime = parseDateString(startDate, LocalTime.MIN);
        ZonedDateTime endDateTime = parseDateString(endDate, LocalTime.MAX);

        // Set range
        String formattedStartDate = formattedDateTime(startDateTime);
        String formattedEndDate = formattedDateTime(endDateTime);

        // Print the range
        System.out.println(String.format("Start Date: %s", formattedStartDate));
        System.out.println(String.format("End Date: %s", formattedEndDate));

        // Get the time entries
        return webTarget
            .path(TIME_ENTRY_URI)
            .queryParam(START_DATE_PARAM, formattedStartDate)
            .queryParam(END_DATE_PARAM, formattedEndDate)
            .request(MediaType.APPLICATION_JSON_TYPE)
            .get(new GenericType<List<TimeEntry>>() {});
    }
}
