package io.gitlab.mdbouchard.toggljirasync.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.net.URI;

@Getter
@ToString
public class TogglConfig {
    @JsonProperty
    private URI uri;

    @JsonProperty
    private String token;
}
