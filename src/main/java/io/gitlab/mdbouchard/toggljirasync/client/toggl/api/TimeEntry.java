package io.gitlab.mdbouchard.toggljirasync.client.toggl.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.time.ZonedDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@ToString
public class TimeEntry {
    @JsonProperty
    private Long id;

    @JsonProperty("pid")
    private Long projectId;

    @JsonProperty
    private String description;

    @JsonProperty
    private ZonedDateTime start;

    @JsonProperty
    private ZonedDateTime stop;

    @JsonProperty
    private Long duration;

    @JsonIgnore
    public boolean hasIssueNumber(final Pattern pattern) {
        return (getIssueNumber(pattern) != null);
    }

    @JsonIgnore
    public String getIssueNumber(final Pattern pattern) {
        if (description != null) {
            final Matcher matcher = pattern.matcher(description);
            if (matcher.matches()) {
                return matcher.group(1);
            }
        }
        return null;
    }
}
