package io.gitlab.mdbouchard.toggljirasync.util

import io.gitlab.mdbouchard.toggljirasync.client.jira.api.Author
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.Fields
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.Issue
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.WorkLog
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

class WorkLogSummarySpec extends Specification {
    @Unroll
    void "test report prints work log details"() {
        when:
        WorkLogSummary.report('Foo', workLogs, processWorkLogClosure)

        then:
        noExceptionThrown()

        where:
        workLogs << [
            null,
            [],
            [
                new WorkLog(
                    issue: new Issue(
                        issueNumber: 'IFB-123',
                        fields: new Fields(
                            summary: 'Test summary'
                        )
                    ),
                    author: new Author(name: 'User Name'),
                    started: LocalDateTime.parse('2018-11-22T22:48:24.325').atZone(ZoneId.systemDefault()),
                    timeSpentSeconds: 10 * 60,
                    comment: 'Test comment'
                )
            ],
            [
                new WorkLog(
                    issue: new Issue(
                        issueNumber: 'IFB-123',
                        fields: new Fields(
                            summary: 'Test summary'
                        )
                    ),
                    author: new Author(name: 'User Name'),
                    started: LocalDateTime.parse('2018-11-22T22:48:24.325').atZone(ZoneId.systemDefault()),
                    timeSpentSeconds: 10 * 60,
                    comment: 'Test comment'
                )
            ]
        ]
        processWorkLogClosure << [
            null,
            null,
            null,
            { assert it instanceof WorkLog && it.issue?.issueNumber == 'IFB-123' }
        ]
    }

    void "test printRow properly prints the row"() {
        given:
        WorkLog workLog = new WorkLog(
            issue: new Issue(
                issueNumber: 'IFB-123',
                fields: new Fields(
                    summary: 'Test summary'
                )
            ),
            author: new Author(name: 'User Name'),
            started: LocalDateTime.parse('2018-11-22T22:48:24.325').atZone(ZoneId.systemDefault()),
            timeSpentSeconds: 10 * 60,
            comment: 'Test comment'
        )

        ByteArrayOutputStream buffer = new ByteArrayOutputStream()
        System.out = new PrintStream(buffer)

        when:
        WorkLogSummary.printRow(workLog)

        then:
        noExceptionThrown()
        assert buffer.toString().trim() == '| IFB-123: Test summary                                          | 2018-11-22T22:48:24.325 | 00:10 | Test comment                                       |'
    }

    void "test formatDateTime properly formats date"() {
        given:
        String dateTimeString = '2018-11-24T23:09:00'
        ZonedDateTime dateTime = LocalDateTime.parse(dateTimeString).atZone(ZoneId.systemDefault())

        expect:
        assert WorkLogSummary.formatDateTime(dateTime) == dateTimeString
    }

    void "test formatDateTime handles NULL"() {
        expect:
        assert WorkLogSummary.formatDateTime(null) == null
    }

    @Unroll
    void "test formatTime is correct"() {
        expect:
        assert WorkLogSummary.formatTime(seconds) == result

        where:
        seconds                     | result
        1                           | '00:00'
        59                          | '00:00'
        60                          | '00:01'
        60 + 1                      | '00:01'
        60 + 59                     | '00:01'
        2 * 60                      | '00:02'
        9 * 60 + 59                 | '00:09'
        10 * 60                     | '00:10'
        30 * 60                     | '00:30'
        45 * 60                     | '00:45'
        60 * 60                     | '01:00'
        2 * 60 * 60                 | '02:00'
        3 * 60 * 60                 | '03:00'
        11 * 60 * 60                | '11:00'
        99 * 60 * 60                | '99:00'
        120 * 60 * 60               | '120:00'
        (120 * 60 * 60) + (45 * 60) | '120:45'
        (120 * 60 * 60) + (59 * 60) | '120:59'
    }

    @Unroll
    void "test truncate properly truncates text"() {
        expect:
        assert WorkLogSummary.truncate(string, maxLength) == expectedString

        where:
        string                   | maxLength | expectedString
        'Test comment goes here' | 25        | 'Test comment goes here'
        'Test comment goes here' | 24        | 'Test comment goes here'
        'Test comment goes here' | 23        | 'Test comment goes here'
        'Test comment goes here' | 22        | 'Test comment goes here'
        'Test comment goes here' | 21        | 'Test comment goes ...'
        'Test comment goes here' | 20        | 'Test comment goes...'
        'Test comment goes here' | 19        | 'Test comment goe...'
        'Test comment goes here' | 18        | 'Test comment go...'
        'Test comment goes here' | 17        | 'Test comment g...'
        'Test comment goes here' | 16        | 'Test comment ...'
        'Test comment goes here' | 15        | 'Test comment...'
        'Test comment goes here' | 14        | 'Test commen...'
        'Test comment goes here' | 13        | 'Test comme...'
        'Test comment goes here' | 12        | 'Test comm...'
        'Test comment goes here' | 11        | 'Test com...'
        'Test comment goes here' | 10        | 'Test co...'
        'Test comment goes here' | 9         | 'Test c...'
        'Test comment goes here' | 8         | 'Test ...'
        'Test comment goes here' | 7         | 'Test...'
        'Test comment goes here' | 6         | 'Tes...'
        'Test comment goes here' | 5         | 'Te...'
        'Test comment goes here' | 4         | 'T...'
        'Test comment goes here' | 3         | '...'
        'Test comment goes here' | 2         | '...'
        'Test comment goes here' | 1         | '...'
    }
}
