package io.gitlab.mdbouchard.toggljirasync.client.jira;

import io.gitlab.mdbouchard.toggljirasync.client.RestClient;
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.ErrorResponse;
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.Issue;
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.WorkLog;
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.WorkLogResult;
import io.gitlab.mdbouchard.toggljirasync.client.provider.PasswordProvider;
import io.gitlab.mdbouchard.toggljirasync.config.JiraConfig;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

public class JiraClient extends RestClient {
    private static final String ISSUE_NUMBER_TEMPLATE = "issueNumber";

    private static final String BASE_URI = "/rest/api/latest";
    private static final String ISSUE_URI = BASE_URI + "/issue/{" + ISSUE_NUMBER_TEMPLATE + "}";
    private static final String WORKLOG_URI = ISSUE_URI + "/worklog";

    public JiraClient(final JiraConfig config, final PasswordProvider passwordProvider) {
        super(config.getUri(), config.getUsername(), passwordProvider.getPassword());
    }

    public Issue getIssue(final String issueNumber) {
        return webTarget
            .path(ISSUE_URI)
            .resolveTemplate(ISSUE_NUMBER_TEMPLATE, issueNumber)
            .queryParam("fields", "summary")
            .request(MediaType.APPLICATION_JSON)
            .get(Issue.class);
    }

    public List<WorkLog> getWorkLogs(final String issueNumber) {
        return Optional.ofNullable(
            webTarget
                .path(WORKLOG_URI)
                .resolveTemplate(ISSUE_NUMBER_TEMPLATE, issueNumber)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(WorkLogResult.class))
            .map(WorkLogResult::getWorklogs)
            .orElse(Collections.emptyList());
    }

    public List<WorkLog> getWorkLogsForUser(final String issueNumber) {
        return getWorkLogsForUser(issueNumber, username);
    }

    public List<WorkLog> getWorkLogsForUser(final String issueNumber, final String username) {
        return getWorkLogs(issueNumber)
            .stream()
            .filter(workLog -> workLog != null && workLog.getAuthor() != null
                && StringUtils.isNotEmpty(workLog.getAuthor().getName()))
            .filter(workLog -> StringUtils.equalsIgnoreCase(username, workLog.getAuthor().getName()))
            .collect(toList());
    }

    public WorkLog addWorkLog(final WorkLog workLog) {
        try {
            if (workLog == null || workLog.getIssue() == null
                || StringUtils.isEmpty(workLog.getIssue().getIssueNumber())) {
                throw new IllegalArgumentException("WorkLog is missing Issue details");
            }

            return webTarget
                .path(WORKLOG_URI)
                .resolveTemplate(ISSUE_NUMBER_TEMPLATE, workLog.getIssue().getIssueNumber())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(workLog, MediaType.APPLICATION_JSON_TYPE), WorkLog.class);
        } catch (ClientErrorException ex) {
            System.err.println(ex.getResponse().readEntity(ErrorResponse.class));
            throw ex;
        }
    }
}
