package io.gitlab.mdbouchard.toggljirasync.client.provider;

import org.apache.commons.lang3.StringUtils;

public class ConsolePasswordProvider implements PasswordProvider {
    private static final String DEFAULT_PROMPT = "Password: ";
    private String password;
    private String prompt;

    public ConsolePasswordProvider(final String password) {
        this(password, DEFAULT_PROMPT);
    }

    public ConsolePasswordProvider(final String password, final String prompt) {
        this.password = password;
        this.prompt = prompt;
    }

    @Override
    public String getPassword() {
        return StringUtils.isNotEmpty(password) ? password : new String(System.console().readPassword(prompt));
    }
}
