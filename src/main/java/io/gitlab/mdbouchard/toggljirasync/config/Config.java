package io.gitlab.mdbouchard.toggljirasync.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Config {
    @JsonProperty
    private TogglConfig toggl;

    @JsonProperty
    private JiraConfig jira;
}
