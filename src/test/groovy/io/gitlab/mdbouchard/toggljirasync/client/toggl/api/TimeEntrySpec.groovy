package io.gitlab.mdbouchard.toggljirasync.client.toggl.api

import spock.lang.Specification
import spock.lang.Unroll

import static io.gitlab.mdbouchard.toggljirasync.client.jira.api.Issue.ISSUE_PATTERN

class TimeEntrySpec extends Specification {

    @Unroll
    def "test getIssueNumber finds '#issueNumber' in '#description'"() {
        given:
        TimeEntry timeEntry = new TimeEntry(description: description)

        when:
        String result = timeEntry.getIssueNumber(ISSUE_PATTERN)

        then:
        assert result == issueNumber

        where:
        issueNumber  | description
        'TIC-123'    | "$issueNumber"
        'tic-123'    | "$issueNumber"
        'TiCkEt-123' | "$issueNumber"
        'TIC-123'    | "$issueNumber: Test"
        'TIC-123'    | "$issueNumber : Test"
        'TIC-123'    | "$issueNumber: Releases TIC-456"
        'TIC-123'    | "$issueNumber: Releases TIC-456 and TIC-789"
        'TIC-123'    | "Test $issueNumber Test ISSUE-456"
    }
}
