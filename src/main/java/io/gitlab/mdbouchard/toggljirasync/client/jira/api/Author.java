package io.gitlab.mdbouchard.toggljirasync.client.jira.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper = true)
public class Author {
    @JsonProperty
    private String name;
}
