package io.gitlab.mdbouchard.toggljirasync.client

import spock.lang.Specification

class RestClientSpec extends Specification {
    void "test that RestClient correctly sets up webTarget"() {
        given:
        URI uri = new URI('http://localhost')
        String username = 'username'
        String password = 'password'

        when:
        RestClient restClient = new RestClient(uri, username, password) {}

        then:
        assert restClient.uri == uri
        assert restClient.username == username
        assert restClient.password == password
        assert restClient.webTarget?.uri == uri
    }
}
