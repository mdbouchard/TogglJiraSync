package io.gitlab.mdbouchard.toggljirasync.client.provider;

public interface PasswordProvider {
    String getPassword();
}
