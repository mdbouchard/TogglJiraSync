package io.gitlab.mdbouchard.toggljirasync.client.jira

import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.junit.WireMockRule
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.Author
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.ErrorResponse
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.Fields
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.Issue
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.WorkLog
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.WorkLogResult
import io.gitlab.mdbouchard.toggljirasync.client.provider.ConsolePasswordProvider
import io.gitlab.mdbouchard.toggljirasync.client.provider.PasswordProvider
import io.gitlab.mdbouchard.toggljirasync.config.JiraConfig
import org.junit.Rule
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import javax.ws.rs.ClientErrorException
import java.time.ZonedDateTime

import static com.github.tomakehurst.wiremock.client.WireMock.badRequest
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo
import static com.github.tomakehurst.wiremock.client.WireMock.get
import static com.github.tomakehurst.wiremock.client.WireMock.ok
import static com.github.tomakehurst.wiremock.client.WireMock.okJson
import static com.github.tomakehurst.wiremock.client.WireMock.post
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import static io.gitlab.mdbouchard.toggljirasync.client.RestClient.OBJECT_MAPPER

class JiraClientSpec extends Specification {
    private static final String USERNAME = 'foo'
    private static final String PASSWORD = 'bar'
    private static final int PORT = 8080

    @Shared
    JiraClient jiraClient

    void setupSpec() {
        JiraConfig jiraConfig = new JiraConfig(
            uri: new URI("http://localhost:${PORT}"),
            username: USERNAME,
            password: PASSWORD
        )
        PasswordProvider passwordProvider = new ConsolePasswordProvider(PASSWORD, 'JIRA Password: ')
        jiraClient = new JiraClient(jiraConfig, passwordProvider)
    }

    @Rule
    WireMockRule wireMock = new WireMockRule(options()
        .port(PORT)
        .notifier(new ConsoleNotifier(true))
    )

    void "test getIssue fetches issue"() {
        given:
        String issueNumber = 'TIC-1234'

        Issue issue = new Issue(
            issueNumber: issueNumber,
            fields: new Fields(
                summary: 'Test summary'
            )
        )

        stubFor(get(urlPathEqualTo("/rest/api/latest/issue/${issueNumber}"))
            .withBasicAuth(USERNAME, PASSWORD)
            .withQueryParam('fields', equalTo('summary'))
            .willReturn(okJson(OBJECT_MAPPER.writeValueAsString(issue)))
        )

        when:
        Issue response = jiraClient.getIssue(issueNumber)

        then:
        noExceptionThrown()
        assert response?.issueNumber == issue.issueNumber
        assert response?.fields?.summary == issue.fields.summary
    }

    void "test getWorkLogs fetches work logs"() {
        given:
        String issueNumber = 'TIC-1234'

        WorkLog workLog = new WorkLog(
            issue: new Issue(
                issueNumber: issueNumber,
                fields: new Fields(
                    summary: 'Test summary'
                )
            ),
            author: new Author(
                name: 'testuser'
            ),
            started: ZonedDateTime.now(),
            timeSpentSeconds: 120,
            comment: 'Test comment'
        )

        WorkLogResult mockResponse = new WorkLogResult(worklogs: [workLog, workLog, workLog])

        stubFor(get(urlEqualTo("/rest/api/latest/issue/${issueNumber}/worklog"))
            .withBasicAuth(USERNAME, PASSWORD)
            .willReturn(okJson(OBJECT_MAPPER.writeValueAsString(mockResponse)))
        )

        when:
        List<WorkLog> response = jiraClient.getWorkLogs(issueNumber)

        then:
        noExceptionThrown()
        assert response?.size() == 3
        response?.each {
            assert it.issue == null
            assert it.author?.name == workLog.author.name
            assert it.started?.isEqual(workLog.started)
            assert it.timeSpentSeconds == workLog.timeSpentSeconds
            assert it.comment == workLog.comment
        }
    }

    void "test getWorkLogs returns empty list for null result"() {
        given:
        String issueNumber = 'TIC-1234'

        stubFor(get(urlEqualTo("/rest/api/latest/issue/${issueNumber}/worklog"))
            .withBasicAuth(USERNAME, PASSWORD)
            .willReturn(ok())
        )

        when:
        List<WorkLog> response = jiraClient.getWorkLogs(issueNumber)

        then:
        noExceptionThrown()
        assert response?.isEmpty()
    }

    @Unroll
    void "test getWorkLogsForUser fetches #resultsFound work log(s) for #username"() {
        given:
        String issueNumber = 'TIC-1234'

        List<WorkLog> workLogs = (1..3).collect {
            new WorkLog(
                issue: new Issue(
                    issueNumber: issueNumber,
                    fields: new Fields(
                        summary: 'Test summary'
                    )
                ),
                author: new Author(
                    name: "testuser${it}"
                ),
                started: ZonedDateTime.now().minusMinutes(it),
                timeSpentSeconds: it,
                comment: "Test comment ${it}"
            )
        }

        WorkLogResult mockResponse = new WorkLogResult(worklogs: workLogs)

        stubFor(get(urlEqualTo("/rest/api/latest/issue/${issueNumber}/worklog"))
            .withBasicAuth(USERNAME, PASSWORD)
            .willReturn(okJson(OBJECT_MAPPER.writeValueAsString(mockResponse)))
        )

        when:
        List<WorkLog> response = jiraClient.getWorkLogsForUser(issueNumber, username)

        then:
        noExceptionThrown()
        assert response?.size() == resultsFound
        workLogs.find { it.author.name.equalsIgnoreCase(username) }?.with { workLog ->
            assert response.first().issue == null
            assert response.first().author?.name == workLog.author.name
            assert response.first().started?.isEqual(workLog.started)
            assert response.first().timeSpentSeconds == workLog.timeSpentSeconds
            assert response.first().comment == workLog.comment
        }

        where:
        username      | resultsFound
        'testuser1'   | 1
        'testuser2'   | 1
        'testuser3'   | 1
        'TesTuser1'   | 1
        'testUSEr2'   | 1
        'TESTUSER3'   | 1
        ' testuser1 ' | 0
        'foo'         | 0
        null          | 0
    }

    void "test addWorkLog saves successfully"() {
        given:
        String issueNumber = 'TIC-1234'

        WorkLog workLog = new WorkLog(
            issue: new Issue(
                issueNumber: issueNumber,
                fields: new Fields(
                    summary: 'Test summary'
                )
            ),
            author: new Author(
                name: 'testuser'
            ),
            started: ZonedDateTime.now(),
            timeSpentSeconds: 120,
            comment: 'Test comment'
        )

        stubFor(post(urlEqualTo("/rest/api/latest/issue/${issueNumber}/worklog"))
            .withBasicAuth(USERNAME, PASSWORD)
            .withRequestBody(equalTo(OBJECT_MAPPER.writeValueAsString(workLog)))
            .willReturn(okJson(OBJECT_MAPPER.writeValueAsString(workLog)))
        )

        when:
        WorkLog response = jiraClient.addWorkLog(workLog)

        then:
        noExceptionThrown()
        assert response.issue == null
        assert response.author?.name == workLog.author.name
        assert response.started?.isEqual(workLog.started)
        assert response.timeSpentSeconds == workLog.timeSpentSeconds
        assert response.comment == workLog.comment
    }

    void "test addWorkLog throws ClientErrorException"() {
        given:
        String issueNumber = 'TIC-1234'

        WorkLog workLog = new WorkLog(
            issue: new Issue(
                issueNumber: issueNumber,
                fields: new Fields(
                    summary: 'Test summary'
                )
            ),
            author: new Author(
                name: 'testuser'
            ),
            started: ZonedDateTime.now(),
            timeSpentSeconds: 120,
            comment: 'Test comment'
        )

        stubFor(post(urlEqualTo("/rest/api/latest/issue/${issueNumber}/worklog"))
            .withBasicAuth(USERNAME, PASSWORD)
            .willReturn(badRequest()
                .withHeader('Content-Type', 'application/json')
                .withBody(OBJECT_MAPPER.writeValueAsString(new ErrorResponse(errorMessages: ['foo', 'bar'])))
            )
        )

        when:
        jiraClient.addWorkLog(workLog)

        then:
        thrown(ClientErrorException)
    }

    @Unroll
    void "test addWorklog validation"() {
        when:
        jiraClient.addWorkLog(workLog)

        then:
        thrown(IllegalArgumentException)

        where:
        workLog << [
            null,
            new WorkLog(),
            new WorkLog(issue: null),
            new WorkLog(issue: new Issue()),
            new WorkLog(issue: new Issue(issueNumber: ''))
        ]
    }
}
