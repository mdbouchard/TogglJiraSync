package io.gitlab.mdbouchard.toggljirasync.util;

public interface ProcessCallback<T> {
    void process(final T workLog);
}
