package io.gitlab.mdbouchard.toggljirasync.mapper;

import io.gitlab.mdbouchard.toggljirasync.client.jira.api.Issue;
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.WorkLog;
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.Project;
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.TimeEntry;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;

public class TimeEntryMapper {
    private final Map<Long, Project> projects = new HashMap<>();

    public TimeEntryMapper(final List<Project> projects) {
        if (projects != null) {
            this.projects.putAll(projects.stream()
                .collect(Collectors.toMap(Project::getId, Function.identity())));
        }
    }

    static String prepareComment(final String issueNumber, final String comment) {
        return Optional.ofNullable(comment)
            .map(str -> str.replaceAll("(?i)" + issueNumber + "\\s*[:;-]?", "")
                .replaceAll("\\s+", " ")
                .trim()
            )
            .orElse(null);
    }

    public static long roundUpOneMinute(final long seconds) {
        return ZonedDateTime
            .ofInstant(Instant.ofEpochSecond(seconds), ZoneId.systemDefault())
            .truncatedTo(ChronoUnit.MINUTES)
            .plusMinutes(1L)
            .toEpochSecond();
    }

    public WorkLog toWorkLog(final Issue issue, final TimeEntry timeEntry) {
        if (issue == null) {
            throw new IllegalArgumentException("Issue cannot be null!");
        }
        if (timeEntry == null || timeEntry.getDuration() == null) {
            throw new IllegalArgumentException("No duration for time entry!");
        }

        final String defaultComment = Optional.ofNullable(projects.get(timeEntry.getProjectId()))
            .map(Project::getName)
            .orElse(null);

        final String comment = defaultIfEmpty(prepareComment(issue.getIssueNumber(), timeEntry.getDescription()),
            defaultComment);

        return WorkLog.builder()
            .issue(issue)
            .started(timeEntry.getStart())
            .timeSpentSeconds(roundUpOneMinute(timeEntry.getDuration()))
            .comment(comment)
            .build();
    }
}
