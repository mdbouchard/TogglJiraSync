package io.gitlab.mdbouchard.toggljirasync.client.toggl.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Workspace {
    @JsonProperty
    private Long id;

    @JsonProperty
    private String name;
}
