package io.gitlab.mdbouchard.toggljirasync.client.provider

import spock.lang.Specification

class ConsolePasswordProviderSpec extends Specification {
    void "test getPassword"() {
        given:
        String password = 'password'
        PasswordProvider passwordProvider = new ConsolePasswordProvider(password, 'prompt')

        expect:
        assert passwordProvider.password == password
    }
}
