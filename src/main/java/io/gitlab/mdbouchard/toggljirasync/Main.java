package io.gitlab.mdbouchard.toggljirasync;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import io.gitlab.mdbouchard.toggljirasync.client.jira.JiraClient;
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.Issue;
import io.gitlab.mdbouchard.toggljirasync.client.jira.api.WorkLog;
import io.gitlab.mdbouchard.toggljirasync.client.provider.ConsolePasswordProvider;
import io.gitlab.mdbouchard.toggljirasync.client.provider.PasswordProvider;
import io.gitlab.mdbouchard.toggljirasync.client.toggl.TogglClient;
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.Project;
import io.gitlab.mdbouchard.toggljirasync.client.toggl.api.TimeEntry;
import io.gitlab.mdbouchard.toggljirasync.config.Config;
import io.gitlab.mdbouchard.toggljirasync.config.JiraConfig;
import io.gitlab.mdbouchard.toggljirasync.exception.ExitApplicationException;
import io.gitlab.mdbouchard.toggljirasync.mapper.TimeEntryMapper;
import io.gitlab.mdbouchard.toggljirasync.util.ProcessCallback;
import io.gitlab.mdbouchard.toggljirasync.util.WorkLogSummary;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.NotFoundException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;

import static io.gitlab.mdbouchard.toggljirasync.util.WorkLogSummary.defaultCommentIfBlank;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class Main {
    public static final String CONFIG_FILE = "config.yml";

    private static final String SAMPLE_CONFIG_FILE = "/" + CONFIG_FILE + ".sample";

    private static final String APP_NAME = "TogglJiraSync";
    private static final String APP_CMD_NAME = "toggljirasync";

    private static final String OPTION_START = "start";
    private static final String OPTION_END = "end";
    private static final String OPTION_DRY_RUN = "dryrun";
    private static final String OPTION_PUBLISH = "publish";
    private static final String OPTION_HELP = "help";

    private static final int HELP_WIDTH = 150;
    private static final String HELP_OPTION_HEADER = null;
    private static final String HELP_OPTION_FOOTER = "\nPlease report issues at https://gitlab.com/mdbouchard/TogglJiraSync/issues\n";
    private static final boolean HELP_OPTION_AUTO_USAGE = true;

    private static final String PROMPT_JIRA_PASSWORD = "JIRA Password: ";
    private static final String MESSAGE_FORMAT_VERSION = "%s: v%s";
    private static final String MESSAGE_FORMAT_ERROR = "Error: %s";

    private static final Integer EXIT_CODE_FAILURE = 1;

    private final TogglClient togglClient;
    private final JiraClient jiraClient;
    private final CommandLine options;

    ///CLOVER:OFF
    private Main(final Config config, final CommandLine options) {
        this(new TogglClient(config.getToggl()), initJiraClient(config.getJira()), options);
    }
    ///CLOVER:ON

    public Main(final TogglClient togglClient, final JiraClient jiraClient, final CommandLine options) {
        this.togglClient = togglClient;
        this.jiraClient = jiraClient;
        this.options = options;
    }

    ///CLOVER:OFF
    public static void main(final String[] args) {
        printVersionInfo();
        try {
            Main main = new Main(readConfig(), parseOptions(args));
            main.findAndPublish();
        } catch (ExitApplicationException ex) {
            System.exit(ex.getExitCode());
        } catch (Exception ex) {
            printError(ex);
            System.exit(EXIT_CODE_FAILURE);
        }
    }
    ///CLOVER:ON

    static void printVersionInfo() {
        System.out.println(String.format(MESSAGE_FORMAT_VERSION, APP_NAME, Main.class.getPackage().getImplementationVersion()));
    }

    static CommandLine parseOptions(final String[] args) {
        // Set up the CLI options
        final Options options = new Options()
            .addOption(Option.builder()
                .longOpt(OPTION_START)
                .hasArg()
                .argName("yyyy-MM-dd")
                .desc("Start date (default: today)")
                .build())
            .addOption(Option.builder()
                .longOpt(OPTION_END)
                .hasArg()
                .argName("yyyy-MM-dd")
                .desc("End date (default: today)")
                .build())
            .addOption(Option.builder()
                .longOpt(OPTION_DRY_RUN)
                .desc("Pretend to publish time entries to JIRA")
                .build())
            .addOption(Option.builder()
                .longOpt(OPTION_PUBLISH)
                .desc("Publish time entries to JIRA")
                .build())
            .addOption(Option.builder()
                .longOpt(OPTION_HELP)
                .desc("Print usage information")
                .build());

        try {
            final CommandLineParser parser = new DefaultParser();
            final CommandLine commandLine = parser.parse(options, args);

            if (commandLine.hasOption(OPTION_HELP)) {
                printHelp(options);
                throw new ExitApplicationException();
            }

            return commandLine;
        } catch (ParseException ex) {
            printError(ex);
            printHelp(options);
            throw new ExitApplicationException(ex);
        }
    }

    private static void printHelp(final Options options) {
        final HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(HELP_WIDTH, APP_CMD_NAME, HELP_OPTION_HEADER, options, HELP_OPTION_FOOTER,
            HELP_OPTION_AUTO_USAGE);
    }

    private static void printError(final Throwable throwable) {
        printError(MESSAGE_FORMAT_ERROR, throwable.getMessage());
    }

    private static void printError(final String format, final Object... args) {
        System.err.println(String.format(format, args));
    }

    static Config readConfig() throws IOException {
        final ObjectMapper mapper = new YAMLMapper();
        final File configFile = new File(CONFIG_FILE);
        if (!configFile.exists()) {
            try (final InputStream sampleConfigStream = Main.class.getResourceAsStream(SAMPLE_CONFIG_FILE)) {
                Files.copy(sampleConfigStream, Paths.get(configFile.getPath()));
            }
            System.out.println("Created config file: " + configFile.getPath());
            throw new ExitApplicationException();
        }
        return mapper.readValue(configFile, Config.class);
    }

    static JiraClient initJiraClient(final JiraConfig config) {
        final PasswordProvider passwordProvider = new ConsolePasswordProvider(config.getPassword(),
            PROMPT_JIRA_PASSWORD);
        return new JiraClient(config, passwordProvider);
    }

    void findAndPublish() {
        final String startTime = StringUtils.trimToNull(options.getOptionValue(OPTION_START));
        final String endTime = StringUtils.trimToNull(options.getOptionValue(OPTION_END));

        final Map<String, List<TimeEntry>> togglTimeEntries = findTogglEntries(startTime, endTime);
        final Map<String, List<TimeEntry>> filteredTimeEntries = filterByJiraWorkLogs(togglTimeEntries);
        final List<WorkLog> workLogs = mapTimeEntriesToWorkLogs(filteredTimeEntries);
        publishWorkLogs(workLogs);
    }

    Map<String, List<TimeEntry>> findTogglEntries(final String startTime, final String endTime) {
        // Find entries in Toggl for the date range requested
        return Optional.ofNullable(togglClient.getTimeEntries(startTime, endTime))
            .orElse(Collections.emptyList())
            .stream()
            .filter(Objects::nonNull)
            .filter(timeEntry -> {
                if (timeEntry.getStop() == null) {
                    // Check for running time entries
                    printError("WARNING: Toggl entry \"%s\" is still running!",
                        defaultCommentIfBlank(timeEntry.getDescription()));
                    return false;
                }
                return true;
            })
            .filter(timeEntry ->
                // Filter out time entries that have time and a ticket number
                timeEntry.getDuration() > 0 && timeEntry.hasIssueNumber(Issue.ISSUE_PATTERN)
            )
            .collect(groupingBy(timeEntry -> timeEntry.getIssueNumber(Issue.ISSUE_PATTERN), toList()));
    }

    Map<String, List<TimeEntry>> filterByJiraWorkLogs(final Map<String, List<TimeEntry>> timeEntries) {
        // Get work logs in JIRA matching the JIRA API user
        // Filter out non-existent issue numbers
        System.out.println("\nChecking work logs...");

        return Optional.ofNullable(timeEntries)
            .orElse(Collections.emptyMap())
            .entrySet()
            .stream()
            .filter(entry -> {
                try {
                    // Get work logs from JIRA
                    final List<WorkLog> workLogs = Optional.ofNullable(jiraClient.getWorkLogsForUser(entry.getKey()))
                        .orElse(Collections.emptyList());

                    // Remove time entries if already logged
                    entry.getValue().removeIf(timeEntry -> workLogs.stream().anyMatch(workLog ->
                            ChronoUnit.MINUTES.between(workLog.getStarted(), timeEntry.getStart()) < 1L
                                && workLog.getTimeSpentSeconds() == TimeEntryMapper.roundUpOneMinute(timeEntry.getDuration())
                        )
                    );

                    return true;
                } catch (NotFoundException ex) {
                    printError("WARNING: Issue %s cannot be found!", entry.getKey());
                    return false;
                }
            }).collect(toMap(Entry::getKey, Entry::getValue));
    }

    List<WorkLog> mapTimeEntriesToWorkLogs(final Map<String, List<TimeEntry>> timeEntries) {
        // Setup TimeEntry -> WorkLog mapper
        List<Project> projects = togglClient.getProjects();
        TimeEntryMapper timeEntryMapper = new TimeEntryMapper(projects);

        // Map TimeEntries -> WorkLogs
        return timeEntries.entrySet().stream()
            .map(entry -> {
                final Issue issue = jiraClient.getIssue(entry.getKey());
                return entry.getValue().stream()
                    .map(timeEntry -> timeEntryMapper.toWorkLog(issue, timeEntry))
                    .collect(toList());
            })
            .flatMap(List::stream)
            .sorted((a, b) -> b.getStarted().compareTo(a.getStarted()))
            .collect(toList());
    }

    void publishWorkLogs(final List<WorkLog> workLogs) {
        if (options.hasOption(OPTION_PUBLISH) || options.hasOption(OPTION_DRY_RUN)) {
            // Process entries
            if (CollectionUtils.isNotEmpty(workLogs)) {
                final ProcessCallback<WorkLog> callback = workLog -> {
                    if (!options.hasOption(OPTION_DRY_RUN)) {
                        try {
                            // Log entry in JIRA
                            jiraClient.addWorkLog(workLog);
                        } catch (ClientErrorException ignored) {
                        }
                    }
                };

                final String message = (options.hasOption(OPTION_DRY_RUN) ? "[DRYRUN] " : "") + "Publishing...";
                WorkLogSummary.report(message, workLogs, callback);
                System.out.println("Done!");
            } else {
                System.out.println("Nothing to do!");
            }
        } else {
            WorkLogSummary.report("Summarizing...", workLogs);
            if (CollectionUtils.isNotEmpty(workLogs)) {
                System.out.println("To publish the above, rerun with: --publish");
            }
        }
    }
}
