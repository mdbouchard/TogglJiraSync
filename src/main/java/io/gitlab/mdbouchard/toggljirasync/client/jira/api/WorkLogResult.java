package io.gitlab.mdbouchard.toggljirasync.client.jira.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@ToString
public class WorkLogResult extends ResultWrapper {
    @JsonProperty
    private List<WorkLog> worklogs = new ArrayList<>();
}
