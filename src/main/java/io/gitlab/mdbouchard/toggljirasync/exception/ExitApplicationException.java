package io.gitlab.mdbouchard.toggljirasync.exception;

import lombok.Getter;

@Getter
public class ExitApplicationException extends RuntimeException {
    private final int exitCode;

    public ExitApplicationException() {
        this(0);
    }

    public ExitApplicationException(final int exitCode) {
        this(null, exitCode);
    }

    public ExitApplicationException(final Throwable cause) {
        this(cause, 0);
    }

    public ExitApplicationException(final Throwable cause, final int exitCode) {
        super(cause);
        this.exitCode = exitCode;
    }
}
